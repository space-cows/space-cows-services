package garden.bots

import garden.bots.models.Cow
import garden.bots.models.CowsCollection
import io.vertx.core.Vertx
import javax.inject.Inject
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/hello")
class Main {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    fun hello(): String {
        return "hello"
    }


}