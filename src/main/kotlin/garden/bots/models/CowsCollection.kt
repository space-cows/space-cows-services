package garden.bots.models

import io.vertx.core.json.JsonObject
import java.security.SecureRandom

object CowsCollection {

  fun randomX(): Double {
    return SecureRandom().nextInt(Parameters.constraints().getDouble("width").toInt()).toDouble()
  }
  fun randomY(): Double {
    return SecureRandom().nextInt(Parameters.constraints().getDouble("height").toInt()).toDouble()
  }
  var cows: List<Cow> = listOf(
    Cow(nickName= "Prudence", x=randomX(), y=randomY(), size=10.0, sex="female"),
    Cow(nickName= "Hazel", x=randomX(), y=randomY(), size=10.0, sex="female"),
    Cow(nickName= "Daisy", x=randomX(), y=randomY(), size=10.0, sex="female"),
    Cow(nickName= "Lilly", x=randomX(), y=randomY(), size=10.0, sex="female"),
    Cow(nickName= "Cinnamon", x=randomX(), y=randomY(), size=10.0, sex="female"),
    Cow(nickName= "Caramelle", x=randomX(), y=randomY(), size=10.0, sex="female"),
    Cow(nickName= "Martha", x=randomX(), y=randomY(), size=10.0, sex="female"),
    Cow(nickName= "Moode", x=randomX(), y=randomY(), size=10.0, sex="female"),
    Cow(nickName= "Button", x=randomX(), y=randomY(), size=10.0, sex="female"),

    Cow(nickName= "Bigfoot", x=randomX(), y=randomY(), size=10.0, sex="male"),
    Cow(nickName= "Vegas", x=randomX(), y=randomY(), size=10.0, sex="male"),
    Cow(nickName= "George", x=randomX(), y=randomY(), size=10.0, sex="male"),
    Cow(nickName= "Kargo", x=randomX(), y=randomY(), size=10.0, sex="male"),
    Cow(nickName= "Chuck", x=randomX(), y=randomY(), size=10.0, sex="male"),
    Cow(nickName= "Boomboom", x=randomX(), y=randomY(), size=10.0, sex="male"),
    Cow(nickName= "Warpath", x=randomX(), y=randomY(), size=10.0, sex="male"),
    Cow(nickName= "Tiny", x=randomX(), y=randomY(), size=10.0, sex="male"),
    Cow(nickName= "Spice", x=randomX(), y=randomY(), size=10.0, sex="male")
  )

  @JvmStatic fun records(): List<Cow> {
    return this.cows
  }

}
