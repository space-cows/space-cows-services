package garden.bots.models

import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject


object Parameters {
  fun constraints(): JsonObject {
    return JsonObject()
      .put("border", 5.0)
      .put("width", 600.0)
      .put("height", 400.0)
      .put("maxVelocity", 5.0)
  }

  fun border(): Double = constraints().getDouble("border")
  fun width(): Double = constraints().getDouble("width")
  fun height(): Double = constraints().getDouble("height")
  fun maxVelocity(): Double = constraints().getDouble("maxVelocity")


}
