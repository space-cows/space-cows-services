package garden.bots.models

import io.vertx.core.Vertx
import io.vertx.core.Vertx.vertx
import io.vertx.core.json.JsonObject


class Cow(
  val nickName: String,
  var x: Double, var y: Double,
  var size: Double = 0.0,
  val sex: String = "female") {

  var xVelocity: Double = 1.0
  var yVelocity: Double = -1.0
  val constraints = Parameters

  val vertx = Vertx.factory.vertx()

  fun move() {
    this.x += this.xVelocity
    this.y += this.yVelocity

    if(this.x <= this.constraints.border() || this.x >= this.constraints.width() - this.constraints.border()) {
      this.x -= this.xVelocity
      this.x = Math.max(this.x, this.constraints.border())
      this.x = Math.min(this.x, this.constraints.width() - this.constraints.border())
      this.xVelocity = -this.xVelocity
      this.x += this.xVelocity
    }

    if(this.y <= this.constraints.border() || this.y >= this.constraints.height() - this.constraints.border()) {
      this.y -= this.yVelocity
      this.y = Math.max(this.y, this.constraints.border())
      this.y = Math.min(this.y, this.constraints.height() - this.constraints.border())
      this.yVelocity = -this.yVelocity
      this.y += this.yVelocity
    }

  }

  fun distance (boid: Cow): Double {
    val distX: Double = this.x - boid.x
    val distY: Double = this.y - boid.y
    return Math.sqrt(distX * distX + distY * distY)
  }


  fun moveAway (boids: List<Cow>, minDistance: Double) {
    var distanceX = 0.0
    var distanceY = 0.0
    var numClose = 0.0

    for(i in 0 .. boids.size-1) {
      val boid = boids[i]

      if(boid.x == this.x && boid.y == this.y) continue

      val distance = this.distance(boid)
      if(distance < minDistance) {
        numClose++;
        var xdiff = (this.x - boid.x)
        var ydiff = (this.y - boid.y)

        if(xdiff >= 0) xdiff = Math.sqrt(minDistance) - xdiff
        else if(xdiff < 0) xdiff = -Math.sqrt(minDistance) - xdiff

        if(ydiff >= 0) ydiff = Math.sqrt(minDistance) - ydiff
        else if(ydiff < 0) ydiff = -Math.sqrt(minDistance) - ydiff

        distanceX += xdiff
        distanceY += ydiff
        //boid = null;
      }
    }

    if(numClose == 0.0) return

    this.xVelocity -= distanceX / 5
    this.yVelocity -= distanceY / 5
  }


  fun moveCloser (boids: List<Cow>, distance: Double) {
    if(boids.size < 1) return

    var avgX = 0.0
    var avgY = 0.0
    for(i in 0 .. boids.size-1) {
      val boid = boids[i]
      if(boid.x == this.x && boid.y == this.y) continue
      if(this.distance(boid) > distance) continue

      avgX += (this.x - boid.x)
      avgY += (this.y - boid.y)
      //boid = null;
    }

    avgX /= boids.size
    avgY /= boids.size

    val new_distance = Math.sqrt((avgX * avgX) + (avgY * avgY)) * -1.0
    if(new_distance == 0.0) return

    this.xVelocity= Math.min(this.xVelocity + (avgX / new_distance) * 0.15, this.constraints.maxVelocity())
    this.yVelocity = Math.min(this.yVelocity + (avgY / new_distance) * 0.15, this.constraints.maxVelocity())

  }

  fun moveWith (boids: List<Cow>, distance: Double) {
    if(boids.size < 1) return

    // calculate the average velocity of the other boids
    var avgX = 0.0
    var avgY = 0.0
    for(i in 0 .. boids.size-1) {
      var boid = boids[i]
      if(boid.x == this.x && boid.y == this.y) continue
      if(this.distance(boid) > distance) continue

      avgX += boid.xVelocity
      avgY += boid.yVelocity
      //boid = null;
    }
    avgX /= boids.size
    avgY /= boids.size

    val new_distance = Math.sqrt((avgX * avgX) + (avgY * avgY)) * 1.0
    if(new_distance == 0.0) return

    this.xVelocity= Math.min(this.xVelocity + (avgX / new_distance) * 0.05, this.constraints.maxVelocity())
    this.yVelocity = Math.min(this.yVelocity + (avgY / new_distance) * 0.05, this.constraints.maxVelocity())
  }

  fun toJson(): JsonObject {
    return JsonObject()
      .put("x", this.x)
      .put("y", this.y)
      .put("nickName", this.nickName)
      .put("sex", this.sex)
      .put("size", this.size)
      .put("xVelocity", this.xVelocity)
      .put("yVelocity", this.yVelocity)
  }

  fun start() {
    println(this.nickName + " is started at x:" + this.x + " y:" + this.y)
    vertx.setPeriodic(1000) {
      this.moveWith(CowsCollection.records(), 300.0)
      this.moveCloser(CowsCollection.records(), 300.0)
      this.moveAway(CowsCollection.records(), 15.0)
      this.move()
      //println(this.nickName + ": " + this.x + " " + this.y)
    }
  }

}

