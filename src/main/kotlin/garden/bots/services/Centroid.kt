package garden.bots.services

import garden.bots.models.Cow
import garden.bots.models.CowsCollection
import io.vertx.core.Vertx
import io.vertx.core.VertxOptions
import io.vertx.core.json.JsonObject
import org.eclipse.microprofile.metrics.MetricUnits
import org.eclipse.microprofile.metrics.annotation.Gauge
import javax.inject.Inject
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType



@Path("/centroid")
class Centroid {

    fun centroidX(): Double = CowsCollection.records().sumByDouble { cow -> cow.x } / CowsCollection.records().size
    fun centroidY(): Double = CowsCollection.records().sumByDouble { cow -> cow.y } / CowsCollection.records().size

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun json(): String {
        return JsonObject().put("centroidX", centroidX()).put("centroidY", centroidY()).encodePrettily()
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/metrics")
    fun openMetrics(): String {

        //val centroidX: Double = CowsCollection.records().sumByDouble { cow -> cow.x } / CowsCollection.records().size
        //val centroidY: Double = CowsCollection.records().sumByDouble { cow -> cow.y } / CowsCollection.records().size

        return """
            # HELP centroid_x of the cows.
            # TYPE centroid_x gauge
            centroid_x ${centroidX()}
            # HELP centroid_y of the cows.
            # TYPE centroid_y gauge
            centroid_x ${centroidY()}            
        """.trimIndent()
    }

}