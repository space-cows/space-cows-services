package garden.bots.services


import garden.bots.models.CowsCollection
import io.vertx.core.json.JsonArray
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/api/cows")
class CowsService {
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  fun cows(): String {
    //println(CowsCollection.records()[0].move())
    //println(CowsCollection.records()[0].toJson())

    val cows = JsonArray()

    CowsCollection.records().forEach { cow ->
      cows.add(cow.toJson())
    }

    return cows.encodePrettily()
  }


}