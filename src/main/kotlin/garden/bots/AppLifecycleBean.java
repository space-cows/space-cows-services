package garden.bots;

import garden.bots.models.Cow;
import garden.bots.models.CowsCollection;
import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

@ApplicationScoped
public class AppLifecycleBean {

  private static final Logger LOGGER = LoggerFactory.getLogger("ListenerBean");

  void onStart(@Observes StartupEvent ev) {
    LOGGER.info("The application is starting...");
    CowsCollection.records().forEach(Cow::start);

  }



  void onStop(@Observes ShutdownEvent ev) {
    LOGGER.info("The application is stopping...");
  }

}