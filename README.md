# Space Cows services

> WIP 🚧

## Run for dev 

```shell
cd hello-world
./mvnw compile quarkus:dev:
```

## Build for deployment

```shell
mvn package
java -jar target/space-cows-demo-1.0-SNAPSHOT-runner.jar 
```

## Prepare deployment

go to `src/main/docker/Dockerfile.jvm` and add `EXPOSE 8080` at the end

## Deploy to OpenShift

```shell 
oc new-build --strategy docker --dockerfile - --code . --name space-cows-demo < src/main/docker/Dockerfile.jvm
# see the builds
# start the builds
oc start-build --from-dir . space-cows-demo
# create an application for that
oc new-app --image-stream pico/space-cows-demo --name space-cows-demo
# expose a service
oc expose svc/space-cows-demo
oc describe route space-cows-demo
```

```
#redeploy
mvn package
oc start-build --from-dir . space-cows-demo
```
